require "enotasgw/version"
require "enotasgw/api_base"
require "enotasgw/endereco"
require "enotasgw/configuracao_nfse"
require "enotasgw/configuracao_nfse_producao"
require "enotasgw/configuracao_nfse_homologacao"
require "enotasgw/certificado_digital"
require "enotasgw/empresa"
require "enotasgw/cliente"
require "enotasgw/servico"
require "enotasgw/nfe"

module EnotasGW
  class << self
    attr_writer :logger

    def logger
      @logger ||= Logger.new($stdout).tap do |log|
        log.progname = self.name
      end
    end
  end

  EnotasGW.logger = Rails.logger if defined?(Rails)
end
