class EnotasGW::Empresa
  include EnotasGW::ApiBase

  attribute :endereco, EnotasGW::Endereco                                        # (Endereco, optional),
  attribute :id, String                                                          # (string): ,
  attribute :status, String                                                      # (string, optional): ,
  attribute :prazo, Integer                                                      # (integer, optional): ,
  attribute :dadosObrigatoriosPreenchidos, Boolean                               # (boolean, optional): ,
  attribute :cnpj, String                                                        # (string): ,
  attribute :inscricaoMunicipal, String                                          # (string): ,
  attribute :inscricaoEstadual, String                                           # (string): ,
  attribute :razaoSocial, String                                                 # (string): ,
  attribute :nomeFantasia, String                                                # (string): ,
  attribute :optanteSimplesNacional, Boolean                                     # (boolean): ,
  attribute :email, String                                                       # (string): ,
  attribute :telefoneComercial, String                                           # (string): ,
  attribute :incentivadorCultural, String                                        # (string): ,
  attribute :regimeEspecialTributacao, String                                    # (string): ,
  attribute :aedf, String                                                        # (string): ,
  attribute :configuracoesNFSeProducao, EnotasGW::ConfiguracaoNfseProducao       # (ConfiguracoesNFSeProducao, optional),
  attribute :configuracoesNFSeHomologacao, EnotasGW::ConfiguracaoNfseHomologacao # (ConfiguracoesNFSeHomologacao, optional),
  attribute :codigoServicoMunicipal, String                                      # (string): ,
  attribute :itemListaServicoLC116, String                                       # (string): ,
  attribute :cnae, String                                                        # (string): ,
  attribute :aliquotaIss, Float                                                  # (number): ,
  attribute :descricaoServico, String                                            # (string, optional): ,
  attribute :enviarEmailCliente, Boolean                                         # (boolean, optional): ,
  attribute :certificadoDigital, EnotasGW::CertificadoDigital                    # (CertificadoDigital, optional)

  def self.find(id)
    response = Typhoeus.get("#{base_url}/#{id}", headers: headers)
    if response.success?
      data = JSON.parse(response.body, symbolize_names: true)
      return self.new(data)
    else
      handle_response(response)
    end
    return nil
  end

  def self.where(parameters={})
    defaults = { pageNumber: 0, pageSize: 10 }
    parameters.reject!{ |key, value| value.blank? }
    opts = defaults.merge(parameters)
    querystring = Addressable::URI.new.tap do |uri|
      uri.query_values = opts
    end.query

    response = Typhoeus.get("#{base_url}?#{querystring}", headers: headers)
    if response.success?
      result = JSON.parse(response.body, symbolize_names: true)
      return result[:data].map{ |record| self.new(record) }
    else
      handle_response(response)
    end
    return nil
  end

  def self.all
    where
  end

  private

    def self.base_url
      api_url + "/empresas"
    end
end
