class EnotasGW::ConfiguracaoNfse
  include EnotasGW::ApiBase

  attribute :sequencialNFe, String
  attribute :serieNFe, String
  attribute :sequencialLoteNFe, String
  attribute :usuarioAcessoProvedor, String
  attribute :tokenAcessoProvedor, String
end
