class EnotasGW::Cliente
  include EnotasGW::ApiBase

  attribute :endereco, EnotasGW::Endereco # (Endereco, optional): Endereço do cliente. Este campo é opcional para algumas prefeituras, para maiores detalhes sobre a sua prefeitura consulte http://portal.enotasgw.com.br/article-categories/prefeituras ,
  attribute :tipoPessoa, String           # (string, optional): "F" para pessoa física e "J" para pessoa jurídica ,
  attribute :nome, String                 # (string, optional): Nome do cliente ,
  attribute :email, String                # (string, optional): Endereço de email ,
  attribute :cpfCnpj, String              # (string, optional): CPF para pessoa física ou CNPJ para pessoa jurídica ,
  attribute :inscricaoMunicipal, String   # (string, optional): Inscrição municipal do cliente, deve ser preenchido apenas para pessoa física ou Jurídica do mesmo município que o Prestador ,
  attribute :inscricaoEstadual, String    # (string, optional): Inscrição estadual do cliente, deve ser preenchido apenas para pessoa Jurídica ,
  attribute :telefone, String             # (string, optional): Telefone do cliente
end
