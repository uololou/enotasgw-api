class EnotasGW::Servico
  include EnotasGW::ApiBase

  attribute :descricao, String                  # (string): Descrição do serviço prestado. ,
  attribute :aliquotaIss, Float                 # (number, optional): Valor da aliquota Iss. Caso não seja informado será considerado o valor padrão configurado no cadastro da empresa (CNPJ emissor). ,
  attribute :issRetidoFonte, Boolean            # (boolean, optional): Indica se o Iss deverá ser retido na fonte. ,
  attribute :cnae, String                       # (string, optional): Código CNAE que identifica o serviço prestado. Caso não seja informado será considerado o valor padrão configurado no cadastro da empresa (CNPJ emissor). ,
  attribute :codigoServicoMunicipio, String     # (string, optional): Código do serviço municipal conforme cadastro na prefeitura. Caso não seja informado será considerado o valor padrão configurado no cadastro da empresa (CNPJ emissor). Este código varia de acordo com a prefeitura, para maiores detalhes consulte http://portal.enotasgw.com.br/article-categories/prefeituras ,
  attribute :descricaoServicoMunicipio, String  # (string, optional): Descrição do serviço municipal conforme cadastro na prefeitura. Caso não seja informado será considerado o valor padrão configurado no cadastro da empresa (CNPJ emissor). Este descrição varia de acordo com a prefeitura, para maiores detalhes consulte http://portal.enotasgw.com.br/article-categories/prefeituras ,
  attribute :itemListaServicoLC116, String      # (string, optional): Item da lista de serviço conforme a Lei Complementar 116 (LC116). Para maiores detalhes consulte http://portal.enotasgw.com.br/article-categories/lista-servicos-lc116 ,
  attribute :ufPrestacaoServico, String         # (string, optional): Sigla do Estado onde o serviço foi prestado. Opcional caso o serviço tenha sido prestado no mesmo munícipio do prestador. ,
  attribute :municipioPrestacaoServico, String  # (string, optional): Nome ou código IBGE do munícipio onde o serviço foi prestado. Opcional caso o serviço tenha sido prestado no mesmo munícipio do prestador. ,
  attribute :valorCofins, Float                 # (number, optional): Valor do COFINS ,
  attribute :valorCsll, Float                   # (number, optional): Valor do CSLL ,
  attribute :valorInss, Float                   # (number, optional): Valor do INSS ,
  attribute :valorIr, Float                     # (number, optional): Valor do IR ,
  attribute :valorPis, Float                    # (number, optional): Valor do PIS
end
