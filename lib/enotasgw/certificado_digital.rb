class EnotasGW::CertificadoDigital
  include EnotasGW::ApiBase

  attribute :nome, String
  attribute :dataVencimento, String
end
