class EnotasGW::Endereco
  include EnotasGW::ApiBase

  attribute :codigoIbgeUf, Integer      # (integer, optional): ,
  attribute :codigoIbgeCidade, Integer  # (integer, optional): ,
  attribute :pais, String               # (string, optional): Sigla do País com 3 dígitos. Exemplo: BRA - Brasil, ARG - Argentina. Para maiores detalhes consulte http://portal.enotasgw.com.br/article-categories/paises. Caso não seja informado será considerado como padrão o valor "BRA". ,
  attribute :uf, String                 # (string, optional): ,
  attribute :cidade, String             # (string, optional): Nome ou código IBGE da cidade ,
  attribute :logradouro, String         # (string, optional): ,
  attribute :numero, String             # (string, optional): ,
  attribute :complemento, String        # (string, optional): ,
  attribute :bairro, String             # (string, optional): ,
  attribute :cep, String                # (string, optional):
end
