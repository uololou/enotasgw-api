class EnotasGW::Nfe
  include EnotasGW::ApiBase

  attribute :numero, String             # (string, optional): Número da Nota Fiscal ,
  attribute :codigoVerificacao, String  # (string, optional): Código de verificação da Nota Fiscal ,
  attribute :dataAutorizacao, String    # (string, optional),
  attribute :chaveAcesso, String        # (string, optional): Chave de acesso da Nota Fiscal ,
  attribute :linkDownloadPDF, String    # (string, optional): Link para download do arquivo PDF da Nota Fiscal ,
  attribute :linkDownloadXML, String    # (string, optional): Link para download do arquivo Xml da Nota Fiscal ,
  attribute :status, String             # (string, optional): Status da Nota Fiscal ,
  attribute :motivoStatus, String       # (string, optional): Motivo do status da Nota Fiscal ,
  attribute :enviadaPorEmail, Boolean   # (boolean, optional): Indica se a Nota Fiscal foi enviada ao cliente por email. ,
  attribute :enviarPorEmail, Boolean    # (boolean, optional): Indica se a Nota Fiscal deve ser enviada ao cliente por email. ,
  attribute :cliente, EnotasGW::Cliente # (Cliente, optional): Cliente para o qual a nota fiscal será emitida. A obrigatoriedade deste campo varia de acordo com a prefeitura, para maiores detalhes consulte http://portal.enotasgw.com.br/article-categories/prefeituras ,
  attribute :id, String                 # (string, optional): Identificador único da Nota Fiscal. Usado apenas em casos de atualização de uma nota fiscal existente ,
  attribute :ambienteEmissao, String    # (string, optional),
  attribute :tipo, String               # (string, optional): Tipo da Nota Fiscal. "NFS-e" para NFe de Serviço e "NF-e" para NFe de Produto ,
  attribute :idExterno, String          # (string, optional): Identificador externo da Nota Fiscal informado pelo consumidor da API ao emitir a Nota Fiscal ,
  attribute :servico, EnotasGW::Servico # (Servico): Detalhamento do serviço prestado ,
  attribute :valorTotal, Float          # (number): Valor total da Nota Fiscal

  def self.find(empresa_id, id)
    response = Typhoeus.get("#{base_url(empresa_id)}/#{id}", headers: headers)
    if response.success?
      data = JSON.parse(response.body, symbolize_names: true)
      return self.new(data)
    else
      handle_response(response)
    end
    return nil
  end

  def self.find_by_external_id(empresa_id, external_id)
    response = Typhoeus.get("#{base_url(empresa_id)}/porIdExterno/#{external_id}", headers: headers)
    if response.success?
      data = JSON.parse(response.body, symbolize_names: true)
      return self.new(data)
    else
      handle_response(response)
    end
    return nil
  end

  def self.where(empresa_id, parameters={})
    defaults = { pageNumber: 0, pageSize: 10, filter: "dataCriacao ge '2000-01-01'" }
    parameters.reject!{ |key, value| value.blank? }
    opts = defaults.merge(parameters)
    querystring = Addressable::URI.new.tap do |uri|
      uri.query_values = opts
    end.query

    response = Typhoeus.get("#{base_url(empresa_id)}?#{querystring}", headers: headers)
    if response.success?
      result = JSON.parse(response.body, symbolize_names: true)
      return result[:data].map{ |record| self.new(record) }
    else
      handle_response(response)
    end
    return nil
  end

  def self.all(empresa_id)
    where(empresa_id)
  end

  def self.create(empresa_id, nfe)
    response = Typhoeus::Request.post(base_url(empresa_id), body: nfe.to_json, headers: headers)
    data = JSON.parse(response.body, symbolize_names: true)
    if response.success?
      object = self.new(data)
    else
      object = self.new(nfe.to_hash)
      object.assign_errors(data) if response.response_code == 422
      handle_response(response)
    end
    return object
  end

  def self.destroy(empresa_id, id)
    response = Typhoeus::Request.delete("#{base_url(empresa_id)}/#{id}", headers: headers)
    handle_response(response)
    return response.success?
  end

  def self.destroy_by_external_id(empresa_id, external_id)
    response = Typhoeus::Request.delete("#{base_url(empresa_id)}/porIdExterno/#{external_id}", headers: headers)
    handle_response(response)
    return response.success?
  end

  def self.pdf(empresa_id, id)
    response = Typhoeus.get("#{base_url(empresa_id)}/#{id}/pdf", headers: headers)
    if response.success?
      return response.body
    else
      handle_response(response)
    end
    return nil
  end

  def self.pdf_by_external_id(empresa_id, external_id)
    response = Typhoeus.get("#{base_url(empresa_id)}/porIdExterno/#{external_id}/pdf", headers: headers)
    if response.success?
      return response.body
    else
      handle_response(response)
    end
    return nil
  end

  def self.xml(empresa_id, id)
    response = Typhoeus.get("#{base_url(empresa_id)}/#{id}/xml", headers: headers)
    if response.success?
      return response.body
    else
      handle_response(response)
    end
    return nil
  end

  def self.xml_by_external_id(empresa_id, external_id)
    response = Typhoeus.get("#{base_url(empresa_id)}/porIdExterno/#{external_id}/xml", headers: headers)
    if response.success?
      return response.body
    else
      handle_response(response)
    end
    return nil
  end

  def self.base_url(empresa_id)
    api_url + "/empresas/" + empresa_id + "/nfes"
  end
end
