require "active_support/concern"
require "active_model"
require "json"
require "typhoeus"
require "virtus"

module EnotasGW::ApiBase
  extend ActiveSupport::Concern

  included do
    require "addressable/uri"
    include Virtus.model
    extend  ActiveModel::Naming
    extend  ActiveModel::Translation
    include ActiveModel::Conversion
    include ActiveModel::Validations
    include ActiveModel::Serializers::JSON

    old_attributes = instance_method(:attributes)
    define_method(:attributes) do
      old_attributes.bind(self).().delete_if { |k, v| v.is_a?(Hash) ? v.compact.empty? : v.nil? }
    end
  end

  def persisted?
    id.present?
  end

  def assign_errors(error_data)
    error_data[:errors].each do |attribute, attribute_errors|
      attribute_errors.each do |error|
        self.errors.add(attribute, error)
      end
    end
  end

  module ClassMethods

  private

    def handle_response response
      if response.timed_out?
        # aw hell no
        log("got a time out")
      elsif response.code == 0
        # Could not get an http response, something's wrong.
        log(response.return_message)
      else
        # Received a non-successful http response.
        log("HTTP request failed: " + response.code.to_s)
        log("HTTP request failed: " + response.body.to_s)
      end
    end

    def log(msg)
      EnotasGW.logger.error msg
    end

    def headers(hs={})
      header = {
        "Authorization" => "Basic #{EnotasGW::API_KEY}",
        "Content-Type" => "application/json",
        "Accept" => "application/json"
      }
      header.merge(hs)
    end

    def api_url
      "https://api.enotasgw.com.br/v1"
    end

  end

end
